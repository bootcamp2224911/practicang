import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {
  frmGroup!: FormGroup;

  constructor(private fb: FormBuilder){}
  
  ngOnInit(): void {
    this.frmGroup = this.formInit();
  }

  formInit(): FormGroup{
    return this.fb.group({
      user: ['',/* agregar validador o validadores*/ ],
      password: ['', /* agregar validador o validadores */]
    })
  }
}
