import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pagenotfound',
  templateUrl: './pagenotfound.component.html',
  styleUrl: './pagenotfound.component.css',
  providers: [Router]
})
export class PagenotfoundComponent {
  private route: Router;

  constructor(route: Router){
     this.route = route;
  }

  goToLogin(){
    this.route.navigate(['/home']);
  }
}
