import { Component, EventEmitter, Input, Output, input } from '@angular/core';
import { PropertiesBtnTabla } from '../../interfaces/botones.interface';
import { ProductDetail } from '../../interfaces/productDetail.interface';


@Component({
  selector: 'app-botonestabla',
  templateUrl: './botonestabla.component.html'
})
export class BotonestablaComponent {
  @Output()
  eventoEliminar = new EventEmitter();

  @Output()
  eventoEditar = new EventEmitter<number>();


  @Input()
  propertiesBtn: PropertiesBtnTabla = {
    labelEditar: 'Editar',
    labelEliminar: 'Eliminar',
    mostrarEditar: true,
    mostrarEliminar: true,
    bootstrapEditar: 'success',
    bootstrapEliminar: 'danger'
  };



  eventEliminar(){
    this.eventoEliminar.emit();

  }

  eventEditar(){
    this.eventoEditar.emit();
  }
}
