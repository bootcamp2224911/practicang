import { Component, OnInit } from '@angular/core';
import { ItemsMenu } from '../../interfaces/menuitems.interface';

@Component({
  selector: 'app-menunav',
  templateUrl: './menunav.component.html'
})
export class MenunavComponent implements OnInit{


  itemsMenu: ItemsMenu[] = [
    {
      idOpcionmenu: 1,
      rolId: 1,
      nombreOpcion: 'Home',
      uriRouter: '/home'
    },
    {
      idOpcionmenu: 1,
      rolId: 2,
      nombreOpcion: 'Home',
      uriRouter: '/home'
    },
    {
      idOpcionmenu: 1,
      rolId: 3,
      nombreOpcion: 'Home',
      uriRouter: '/home'
    },
    {
      idOpcionmenu: 2,
      rolId: 2,
      nombreOpcion: 'Products',
      uriRouter: '/products'
    }

  ];

  idroluser: number = 2;
  // OBJETO CON LAS OPCIONES DE ACUERDO AL ROL
  lmenuRol: ItemsMenu[] = [];
  ngOnInit(): void {
    // ASIGNACION DE OPCIONES AL MENU
    this.lmenuRol = this.itemsMenu.filter(itemmenu => itemmenu.rolId === this.idroluser);
  }


}
