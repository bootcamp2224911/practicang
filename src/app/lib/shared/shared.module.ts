import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BotonestablaComponent } from './botonestabla/botonestabla.component';
import { MenunavComponent } from './menunav/menunav.component';
import { RouterModule } from '@angular/router';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';



@NgModule({
  declarations: [
    BotonestablaComponent,
    MenunavComponent,
    PagenotfoundComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports:[
    BotonestablaComponent,
    MenunavComponent, 
    PagenotfoundComponent
  ]
})
export class SharedModule { }
