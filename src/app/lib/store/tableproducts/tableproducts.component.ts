import { Component, ElementRef, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { ProductDetail } from '../../interfaces/productDetail.interface';
import { PropertiesBtnTabla } from '../../interfaces/botones.interface';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-tableproducts',
  templateUrl: './tableproducts.component.html'
})
export class TableproductsComponent{


  @ViewChild('botones', { static: false })
  botones!: ElementRef;
  
  productsList: ProductDetail[] = [
    {
      idProducto: 1,
      producto: 'Pc Gammer',
      modelo: 'Asus XX33-24',
      precio: 452.89
    },
    {
      idProducto: 2,
      producto: 'Monitor UHD',
      modelo: '2PPLG',
      precio: 180.00
    }
];
@Output()
eventEliminar= new EventEmitter<ProductDetail>();

@Output() eventEditar = new EventEmitter<number>();
constructor(private router: Router) {}

btnTablaProperties : PropertiesBtnTabla = {
  mostrarEditar: true,
  mostrarEliminar: true,
  labelEliminar: 'Quitar',
  labelEditar: 'Editar',
  bootstrapEditar: 'success',
  bootstrapEliminar: 'danger'

};




eventoEditar(idProducto: number) {
  this.router.navigate(['/edit-product', idProducto]);
}


  eventoEliminar(product: ProductDetail){
    this.eventEliminar.emit(product);
    let posicionProducto = this.productsList.indexOf(product);
    this.productsList.splice(posicionProducto, 1);
    this.validarproductos();
  }


validarproductos() {
  if (this.productsList.length === 0) {
    this.botones.nativeElement.style.display = 'flex';
  } else {
    this.botones.nativeElement.style.display = 'none';
  }
}
}


