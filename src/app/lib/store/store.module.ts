import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StoreRoutingModule } from './store-routing.module';
import { TableproductsComponent } from './tableproducts/tableproducts.component';
import { SharedModule } from '../shared/shared.module';
import { ProductsComponent } from './products/products.component';


@NgModule({
  declarations: [
    ProductsComponent,
    TableproductsComponent,

  ],
  imports: [
    CommonModule,
    StoreRoutingModule,
    SharedModule
  ],
  exports:[
    ProductsComponent,
    TableproductsComponent
  ]
})
export class StoreModule { }
