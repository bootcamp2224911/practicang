import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductsComponent } from './lib/store/products/products.component';
import { PagenotfoundComponent } from './lib/shared/pagenotfound/pagenotfound.component';


const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'home' },
  { path: 'home', loadChildren: () => import('./lib/general/general.module').then(m => m.GeneralModule) },
  { path: 'products', component: ProductsComponent },
  { path: '**', component: PagenotfoundComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
